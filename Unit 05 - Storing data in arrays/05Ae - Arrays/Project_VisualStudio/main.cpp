#include <iostream>
#include <string>
using namespace std;

void Program1()
{
    int MAX_COURSES = 6;
    int courseCount = 0;
    string myCourses[6];

    for (courseCount = 0; courseCount < MAX_COURSES; courseCount++)
    {
        string userInput;

        cout << "Enter a class name: ";

        cin >> userInput;

        if (userInput == "STOP")
        {
            break;
        }
        else
        {
            myCourses[courseCount] = userInput;
        }
    }

    for (int i = 0; i < courseCount; i++)
    {
        cout << myCourses[i] << "\t" << "  ";
            
    }

    cout << endl;
    cout << endl;
}

void Program2()
{
    string names[5];
    float prices[5];

    for (int i = 0; i < 4; i++)
    {
        cout << "ITEM " << i + 1 << endl;
        cout << "  Enter the item's name: ";
        cin >> names[i];
        
        cout << "  Enter the item's price: ";
        cin >> prices[i];
        cout << endl;
    }

    cout << "ITEMS FOR SALE";
    cout << endl << endl;

    for (int i = 0; i < 4; i++)
    {
        cout << endl;

        cout << names[i] << ": " << prices[i] << endl;
        
    }

    cout << endl << endl;
}

void Program3()
{
    const int WIDTH = 30;
    const int HEIGHT = 10;
    string gameMap[WIDTH][HEIGHT];

    for (int y = 0; y < HEIGHT; y++)
    {
        for (int x = 0; x < WIDTH; x++)
        {
            gameMap[x][y] = ".";
            
            if (x == 0 || x == 29)
            {
                gameMap[x][y] = "#";
            }
            if (y == 0 ||y == 9)
            {
                gameMap[x][y] = "#";
            }
            
            cout << gameMap[x][y];
        }
        cout << endl;
    }
    
    cout << endl;
}

int main()
{
    bool done = false;

    while (!done)
    {
        int choice;
        cout << "0. QUIT" << endl;
        cout << "1. Program 1: Course names" << endl;
        cout << "2. Program 2: Item and price" << endl;
        cout << "3. Program 3: Map file" << endl;
        cout << endl << ">> ";
        cin >> choice;

        switch (choice)
        {
        case 0: done = true; break;
        case 1: Program1(); break;
        case 2: Program2(); break;
        case 3: Program3(); break;
        }
    }

    return 0;
}
