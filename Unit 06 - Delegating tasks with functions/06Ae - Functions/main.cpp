#include <iostream>     
#include <string>       
using namespace std;

#include "functions.hpp"
#include "tests.hpp"

int main()
{
    bool done = false;
    int userChoice;
    while (!done)
    {
        DisplayMenu();
        userChoice = GetChoice(0, 4);

        if (userChoice == 0)
        {
            RunTests();
        }
        else if (userChoice == 1)
        {
            cout << "---------------------------------------" << endl << endl << "PercentToDecimal" << endl << endl;

            float percent;

            cout << endl << "Enter a percentage (no%): ";
            cin >> percent;

            float decimal = PercentToDecimal(percent);

            cout << "Decimal: " << decimal << endl;
        }
        else if (userChoice == 2)
        {
            cout << "---------------------------------------" << endl << endl << "PricePlusTax" << endl << endl;

            float price, tax;

            cout << "Enter price: ";
            cin >> price;

            cout << "Enter tax: ";
            cin >> tax;

            float ppt = PricePlusTax(price, tax);

            cout << endl << "Price plus tax: " << ppt << endl;
        }
        else if (userChoice == 3)
        {
            cout << "---------------------------------------" << endl << endl << "CoutChange" << endl << endl;

            int quarters, dimes, nickels, pennies;

            cout << "Enter # of quarters: ";
            cin >> quarters;
            cout << "Enter # of dimes: ";
            cin >> dimes;
            cout << "Enter # of nickles: ";
            cin >> nickels;
            cout << "Enter # of pennies: ";
            cin >> pennies;

            float dollarAmount = CountChange(quarters, dimes, nickels, pennies);

            cout << "Total money : " << dollarAmount << endl;
        }
        else if (userChoice == 4)
        {
            done = true;
        }
    }

    return 0;
}