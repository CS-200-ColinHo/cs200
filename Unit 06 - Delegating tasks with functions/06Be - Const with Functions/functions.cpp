#include "functions.hpp"

#include <iostream>
using namespace std;

void DisplayMainMenu(int petCount)
{
    cout << endl << "MAIN MENU" << endl;
    cout << endl << "(" << petCount << " pets)" << endl;
    cout << "1. Initialize pets" << endl;
    cout << "2. Add new pet" << endl;
    cout << "3. Display pets" << endl;
    cout << "4. Quit" << endl;


}

int GetChoice(int min, int max)
{
    int userChoice;

    cout << "Please enter a number: ";
    cin >> userChoice;

    while (userChoice < min || userChoice > max)
    {
        cout << "Invalid selection, please try again.";
        cout << "(Enter a number between 0 and 4): ";
        cin >> userChoice;
    }

    return userChoice;
}

void InitializePets(string pets[], int maxPets, int& petCount)
{
    cout << "How many cats do you have? (0-10): ";
    cin >> petCount;

    while (petCount < 0 || petCount > maxPets)
    {
        cout << "Invalid amount, try again: ";
        cin >> petCount;
    }

    cin.ignore();
    for (int i = 0; i < petCount; i++)
    {
        cout << "Enter name for pet # " << i << ": ";
        getline(cin, pets[i]);
    }
}

void DisplayPets(const string pets[], int petCount)
{
    for (int i = 0; i < petCount; i++)
    {
        cout << pets[i] << endl;
    }
}

void AddPet(string pets[], int maxPets, int& petCount)
{
    cout << "Enter new pet name: ";
 
    if (petCount + 1 >= maxPets)
    {
        cout << "ERROR: Array is full" << endl;
    }
    else
    {
        cin.ignore();
        getline(cin, pets[petCount]);
        petCount++;
    }
}
